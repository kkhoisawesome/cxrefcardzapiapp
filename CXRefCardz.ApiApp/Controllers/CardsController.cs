﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using CXRefCardz.ApiApp.Models;
using Swashbuckle.Swagger.Annotations;

namespace CXRefCardz.ApiApp.Controllers
{
    public class CardsController : ApiController
    {
        public CXRefCardzDbContext Context;

        public CardsController()
        {
            Context = Startup.Resolve<CXRefCardzDbContext>();
        }

        public DbSet<Cards> EntityContext
        {
            get
            {
                return Context.Cards;
            }
        }

        public async Task<List<Cards>> GetEntities()
        {
            return await EntityContext.ToListAsync().ConfigureAwait(false);
        }

        [HttpGet]
        [Route("~/cards")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(IEnumerable<Cards>))]
        public async Task<List<Cards>> Get()
        {
            return await Task.Run(() => EntityContext.ToListAsync()).ConfigureAwait(false);
        }
        [HttpGet]
        [Route("~/cards/{id}")]
        public async Task<Cards> Get(string id)
        {
            var entities = await GetEntities();
            return entities.FirstOrDefault(x => x.Id.ToString().ToLower() == id.ToString().ToLower());
        }


        [Authorize]
        [HttpPost]
        [Route("~/cards")]
        [SwaggerResponse(HttpStatusCode.OK, Description = "OK", Type = typeof(bool))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Entity not found", Type = typeof(bool))]
        [SwaggerResponse(HttpStatusCode.Conflict, Description = "Entity already exists", Type = typeof(bool))]
        public async Task<HttpResponseMessage> Post([FromBody] Cards entity)
        {
            if (entity == null)
            {
                return Request.CreateResponse<Cards>(HttpStatusCode.BadRequest, null);
            }

            var entities = await GetEntities();
            bool exists = entities.Any(x => x.Id == entity.Id);
            if (exists)
            {
                return Request.CreateResponse<Cards>(HttpStatusCode.Conflict, null);
            }
            entity.Id = Guid.NewGuid().ToString();
            EntityContext.Add(entity);
            try
            {
                await Context.SaveChangesAsync(new CancellationToken());
            }
            catch (DbUpdateConcurrencyException ex)
            {
                var objContext = ((IObjectContextAdapter)Context).ObjectContext;
                var entry = ex.Entries.Single();
                objContext.Refresh(RefreshMode.ClientWins, entry.Entity);
            }
            return Request.CreateResponse<Cards>(HttpStatusCode.Created, entity);
        }

        [Authorize]
        [HttpPut]
        [Route("~/cards")]
        [SwaggerResponse(HttpStatusCode.OK, Description = "OK", Type = typeof(bool))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Entity not found", Type = typeof(bool))]
        [SwaggerResponse(HttpStatusCode.BadRequest, Description = "Entity sent is null", Type = typeof(bool))]
        public async Task<HttpResponseMessage> Put([FromBody] Cards entity)
        {
            if (entity == null)
            {
                return Request.CreateResponse<bool>(HttpStatusCode.BadRequest, false);
            }

            var entities = await GetEntities();
            if (entities.Any(x => x.Id == entity.Id))
            {
                EntityContext.Attach(entity);
                Context.Entry(entity).State = EntityState.Modified;
                await Context.SaveChangesAsync(new CancellationToken());
                return Request.CreateResponse<bool>(HttpStatusCode.OK, true);
            }
            else
            {
                return Request.CreateResponse<bool>(HttpStatusCode.NotFound, false);
            }
        }

        [Authorize]
        [HttpDelete]
        [SwaggerResponse(HttpStatusCode.OK, Description = "OK", Type = typeof(bool))]
        [SwaggerResponse(HttpStatusCode.NotFound, Description = "Entity not found", Type = typeof(bool))]
        [Route("~/cards/{id}")]
        public async Task<HttpResponseMessage> Delete([FromUri]string id)
        {
            var entities = await GetEntities();

            if (!entities.Any(x => x.Id.ToString().ToLower() == id.ToString().ToLower()))
            {
                return Request.CreateResponse<bool>(HttpStatusCode.NotFound, false);
            }
            else
            {
                EntityContext.Remove(entities.FirstOrDefault(x => x.Id.ToString().ToLower() == id.ToString().ToLower()));
                await Context.SaveChangesAsync(new CancellationToken());
                return Request.CreateResponse<bool>(HttpStatusCode.OK, true);
            }
        }
    }
}