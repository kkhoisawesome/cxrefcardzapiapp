﻿using System;
using System.Collections.Generic;
using System.Linq;
using CXRefCardz.ApiApp.Models;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(CXRefCardz.ApiApp.Startup))]
namespace CXRefCardz.ApiApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            InitializeDependencyInjections();
            //ConfigureAuth(app);
        }
    }
}
