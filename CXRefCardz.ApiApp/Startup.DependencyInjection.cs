﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CXRefCardz.ApiApp.Models;
using Microsoft.Practices.Unity;

namespace CXRefCardz.ApiApp
{
    public partial class Startup
    {
        public static UnityContainer UnityContainer { get; set; }

        private static void InitializeDependencyInjections()
        {
            UnityContainer = new UnityContainer();
            UnityContainer.RegisterType(typeof(CXRefCardzDbContext), typeof(CXRefCardzDbContext), new TransientLifetimeManager());
        }

        public static void RegisterInstance<T>(T item) where T : class
        {
            UnityContainer.RegisterInstance(item);
        }

        public static T Resolve<T>() where T : class
        {
            return UnityContainer.Resolve<T>();
        }
    }
}