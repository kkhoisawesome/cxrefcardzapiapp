﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using CXRefCardz.ApiApp.App_Start;
using CXRefCardz.ApiApp.Models;

namespace CXRefCardz.ApiApp
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            Database.SetInitializer<CXRefCardzDbContext>(null);
            GlobalConfiguration.Configure(WebApiConfig.Register);

        }
    }
}
