﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CXRefCardz.ApiApp.Models
{
    public class Cards
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string Authors { get; set; }
        public string Image { get; set; }
        public string DescriptionInHtml { get; set; }
    }
}