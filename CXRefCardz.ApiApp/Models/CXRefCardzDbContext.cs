﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity.EntityFramework;

namespace CXRefCardz.ApiApp.Models
{
    public class CXRefCardzDbContext : DbContext
    {
        private const string connectionStringName = "Name=DefaultConnection";

        public CXRefCardzDbContext() : base(connectionStringName)
        {
        }
        public static CXRefCardzDbContext Create()
        {
            return new CXRefCardzDbContext();
        }

        public DbSet<Cards> Cards { get; set; }
    }
}